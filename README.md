## ROS panda capacity package and catkin
This is an example catkin workspace for panda capacity calculation. In the this directory you can find the implementation of the python capacity module for a specific use case of Panda robot. The directory consists of two ros packages:
- franka_description: Panda robot definitions from Franka Emika  http://wiki.ros.org/franka_description
- **panda_capacity: the capacity solver for Panda robot**

## Installation - using catkin
To run panda robot capacity calculation nodes first clone the repository and submodules:
```shell
git clone --recursive https://gitlab.inria.fr/auctus-team/people/antunskuric/ros/catkin_workspaces/panda_capacity_ws.git
cd panda_capacity_ws
```

### Dependencies
For visualizing the polytopes in RVIZ you will need to install the [jsk-rviz-plugin](https://github.com/jsk-ros-pkg/jsk_visualization)

```sh
sudo apt install ros-$ROS_DISTRO-jsk-rviz-plugins 
```

The code additionally depends on the pinocchio library. 

#### Installing Pinocchio using system libraries
```bash
sudo apt install ros-$ROS_DISTRO-pinocchio
```
If you do already have pinocchio installed simply install the the pip package `pycapacity`
```
pip install pycapacity
```
And you're ready to go!

#### Installing pinocchio using conda

If you dont have pinocchio installed we suggest you to use Anaconda. Create the environment will all dependencies:

```bash
conda env create -f env.yaml 
conda activate panda_capacity  # activate the environment
```

Once the environment is activated you are ready to go.


### Building the workspace
And then just build the workspace
```shell
cd ..
catkin build
```
And you should be ready to go!

# panda_capacity package

`panda_capacity` package is an example implementation of the task space capacity calculation for for Franka Emika Panda robot in a form of a catkin ros package.

It uses the library [pinocchio](https://github.com/stack-of-tasks/pinocchio) for calculating robot kinematics.


# One panda simulation
Once when you have everything installed you will be able to run the interactive simulations and see the polytope being visualised in real-time.

<img src="https://gitlab.inria.fr/auctus-team/people/antunskuric/ros_nodes/panda_capacity/-/raw/master/images/one_panda.png" height="250px">

To see a simulation with one panda robot and its force and velocity manipulatibility ellipsoid and polytope run the command in the terminal.

```shell
source ~/capacity_ws/devel/setup.bash 
roslaunch panda_capacity one_panda.launch
```

> If using anaconda, don't forget to call <br>
> `conda activate panda_capacity` <br> 
> before you call `roslaunch`
